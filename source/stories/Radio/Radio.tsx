import React from "react";
import getClass from "../../modules/getClass";
import Flex from "../Flex/Flex";
import { RadioProps } from "./Radio.types";
import "./Radio.scss";

const Radio: React.FC<RadioProps> = ({ className, onClick, radioID, groupID, label, checked }) => {
	
	return (
		<Flex 
			onClick={() => onClick ? onClick(groupID, radioID) : null} 
			className={getClass("radio", className)} direction="horizontal"
		>
			<input
				type="radio"
				className={"radioinput"}
				radioGroup={groupID}
			/>
			<div 
				className={!checked ? "radio__dot" : "radio__dot radio__dot_checked"}
			/>
			<div 
				className={!checked ? "radio__label" : "radio__label radio__label_checked"}
			>
				{label}
			</div>
		</Flex>
		
	  
  );
}

export default Radio;