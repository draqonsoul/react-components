export interface RadioProps {
  className?: string;
  onClick?: any;
  radioID: string;
  groupID: string;
  label: string;
  checked: boolean;
}