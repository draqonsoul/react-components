import React, { useState } from "react";
import Radio from './Radio';

export default {
  title: "Inputs/Radio"
};

const Wrapper = () => {
  const [activeRadio, setActiveRadio] = useState("rad1")
  const setRadio = (groupID: any, radioID: any) => {
    if(radioID === activeRadio) setActiveRadio("none")
    else setActiveRadio(radioID)
  }

  return (
    <div>
      <Radio onClick={setRadio} checked={"rad1" === activeRadio ? true : false} radioID="rad1" groupID="radios1" label="hey"/>
      <Radio onClick={setRadio} checked={"rad2" === activeRadio ? true : false} radioID="rad2" groupID="radios1" label="heyB"/>
      <Radio onClick={setRadio} checked={"rad3" === activeRadio ? true : false} radioID="rad3" groupID="radios1" label="heyC"/>
    </div>
  )
}

export const Default = () => <Wrapper />