import React from "react";
import getClass from "../../modules/getClass";
import { CodeProps } from "./Code.types";
import "./Code.scss";

const Code: React.FC<CodeProps> = ({ className, children }) => {
	
	return (
		<code 
			className={getClass("code", className)}
		>
			{children}
		</code>
  );
}

export default Code;