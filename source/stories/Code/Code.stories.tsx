import React from "react";
import Code from './Code';
import snippet__transpiled from "./snippets/main__transpiled.js"

export default {
    title: "Components/Code"
};

export const Default = () => <Code>{snippet__transpiled}</Code>