const snippet__transpiled = <span className='codedata'>
{`from helper.file `}<span className="codedata__variable">import</span>{` File
from os `}<span className="codedata__variable">import</span>{` makedirs, getcwd
from os.path `}<span className="codedata__variable">import</span>{` isdir
from argparse `}<span className="codedata__variable">import</span>{` ArgumentParser
`}<span className="codedata__variable">import</span>{` json


def main():
    print("[Component Templates] Main Script Started")

    default_params_file = File("config/default_params", ".json")
    `}<span className="codedata__conditional">if</span>{` default_params_file.doesExist():
        json_object = json.loads(default_params_file.read())
        parser = ArgumentParser()
        parser.add_argument("-c", "--component", dest="componentName", help="", default=json_object["component_name"])
        parser.add_argument("-p", "--path", dest="outputPath", help="", default=json_object["output_directory"])
        parser.add_argument("-t", "--template", dest="templateType", help="", default=json_object["template_version"])
        args = parser.parse_args()

    `}<span className="codedata__conditional">if</span>{`(args.templateType == "react"):
        templateFiles = [
            {"id": 0, "name": "react", "extension": ".jsx"}, 
            {"id": 1, "name": "react", "extension": ".scss"}
        ]
    `}<span className="codedata__conditional">if</span>{`(args.templateType == "react-typescript"):
        templateFiles = [
            {"id": 0, "name": "react", "extension": ".tsx"}, 
            {"id": 1, "name": "react", "extension": ".scss"}
        ]
    `}<span className="codedata__conditional">if</span>{`(args.templateType == "storybook"):
        templateFiles = [
            {"id": 0, "name": "storybook", "extension": ".jsx"}, 
            {"id": 1, "name": "storybook", "extension": ".stories.jsx"},
            {"id": 2, "name": "storybook", "extension": ".scss"}
        ]
    `}<span className="codedata__conditional">if</span>{`(args.templateType == "storybook-typescript"):
        templateFiles = [
            {"id": 0, "name": "storybook", "extension": ".tsx"}, 
            {"id": 1, "name": "storybook", "extension": ".stories.tsx"},
            {"id": 2, "name": "storybook", "extension": ".types.ts"},
            {"id": 3, "name": "storybook", "extension": ".scss"}
        ]
    `}<span className="codedata__conditional">if</span>{` args.templateType == "react" or args.templateType == "react-typescript" or args.templateType == "storybook" or args.templateType == "storybook-typescript":
        `}<span className="codedata__loop">for</span>{` mappedFile `}<span className="codedata__loop">in</span>{` templateFiles:
            templateFile = File("templates/"+args.templateType+"/"+mappedFile["name"], mappedFile["extension"])
            `}<span className="codedata__conditional">if</span>{` templateFile.doesExist():
                data = templateFile.read()
                data = data.replace("ComponentName", args.componentName)
                #scssname = args.componentName[0].lower() + args.componentName[:1]
                #data = data.replace("componentname", scssname)
                `}<span className="codedata__conditional">try</span>{`:
                    `}<span className="codedata__conditional">if</span>{` not isdir(getcwd() + "\\" + args.outputPath + "\\" + args.componentName):
                        makedirs(getcwd() + "\\" + args.outputPath + "\\" + args.componentName)
                    outputFile = File(args.outputPath + args.componentName + "/" +  args.componentName, mappedFile["extension"])
                    outputFile.write(data)
                `}<span className="codedata__conditional">except</span>{`:
                    print("[ERROR] The specified output path (-p|--path) does not exist!")
                    `}<span className="codedata__loop">break</span>{`;
            `}<span className="codedata__conditional">else</span>{`:
                print("[ERROR] The specified template (-t|--template) does not exist!")
                `}<span className="codedata__loop">break</span>{`;

    print("[Component Templates] Main Script Ended")


`}<span className="codedata__conditional">if</span>{` __name__ == "__main__":
    main()
`}
</span>;

export default snippet__transpiled;