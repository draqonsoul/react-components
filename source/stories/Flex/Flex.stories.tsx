import React from "react";
import Flex from './Flex';
import Image from "../Image/Image";

export default {
  title: "Container/Flex"
};

export const Horizontal = () => <Flex direction="horizontal"> 
<Image scale="scale_down" bordered="bordered" src="images/w_dragon.jpg"/>
<Image scale="scale_down" bordered="bordered" src="images/w_dragon.jpg"/>
<Image scale="scale_down" bordered="bordered" src="images/w_dragon.jpg"/>
</Flex>;

export const Vertical = () => <Flex direction="vertical"> 
<Image scale="scale_down" bordered="bordered" src="images/w_dragon.jpg"/>
<Image scale="scale_down" bordered="bordered" src="images/w_dragon.jpg"/>
<Image scale="scale_down" bordered="bordered" src="images/w_dragon.jpg"/>
</Flex>;