import React from "react";
import getClass from "../../modules/getClass";
import { FlexProps } from "./Flex.types";
import "./Flex.scss";

const Flex: React.FC<FlexProps> = ({ className, children, direction, inline, onClick }) => {

	return (
		<div 
			className={getClass("flex", className, [[direction]])}
			onClick={onClick}
		>
			{children.map((child, key) => child)}
			
		</div>
  );
}

export default Flex;