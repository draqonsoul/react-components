export interface FlexProps {
  className?: string;
  onClick?: any;
  children?: any;
  direction: "horizontal" | "vertical";
  inline?: "inline";
}