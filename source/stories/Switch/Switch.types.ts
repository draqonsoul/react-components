export interface SwitchProps {
  className?: string;
  value: boolean;
  update?: any;
}