import React, { useState } from "react";
import Switch from './Switch';

export default {
    title: "Inputs/Switch"
};

const Wrapper: any = () => {

    const [activeSwitch, setSwitch] = useState(false)

    return (
        <Switch
            update={(val) => setSwitch(val)} 
            value={activeSwitch} 
        />
    )
}

export const Default = () => <Wrapper />