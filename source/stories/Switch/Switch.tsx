import React from "react";
import getClass from "../../modules/getClass";
import { SwitchProps } from "./Switch.types";
import "./Switch.scss";

const Switch: React.FC<SwitchProps> = ({ className, update, value }) => {

	return (
		<label 
			onChange={() => update(!value) } 
			className={getClass("switch", className)}
		>
			<input 
				readOnly={true} 
				type="checkbox" 
				checked={value} 
			/>
			<span 
				className="slider round"
			>
			</span>
		</label>
  );
}

export default Switch;