export interface LinkProps {
  className?: string;
  children: any;
  href: string;
  size?: "teaser" | "medium";
  target: "_blank"|"_self"|"_parent"|"_top"|string;
}