import React from "react";
import Link from './Link';
import Paragraph from '../Paragraph/Paragraph';

export default {
  title: "Components/Link"
};

export const Default = () => <Paragraph size="medium"><Link href="https://google.com" target="_blank">Click Here</Link> to visit Google.com</Paragraph>;