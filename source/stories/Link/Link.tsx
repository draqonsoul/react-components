import React from "react";
import getClass from "../../modules/getClass";
import { LinkProps } from "./Link.types";
import "./Link.scss";

const Link: React.FC<LinkProps> = ({ className, children, href, target, size }) => {
	
	return (
		<a 
			href={href} 
			target={target} 
			className={getClass("link", className, [[size]])}
		>
			{children}
		</a>
  );
}

export default Link;