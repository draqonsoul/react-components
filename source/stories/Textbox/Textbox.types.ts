export interface TextboxProps {
  className?: string;
  value: string;
  update?: any;
  placeholder?: any;
}