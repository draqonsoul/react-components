import React, { useState } from "react";
import Textbox from './Textbox';

export default {
    title: "Inputs/Textbox"
};

const Wrapper: any = () => {

    const [value, setValue] = useState("")

    return (
        <Textbox
            value={value}
            update={(val) => setValue(val)}
            placeholder="Enter some text"
        />
    )
}

export const Default = () => <Wrapper />;