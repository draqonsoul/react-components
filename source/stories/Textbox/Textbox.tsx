import React from "react";
import getClass from "../../modules/getClass";
import { TextboxProps } from "./Textbox.types";
import "./Textbox.scss";

const Textbox: React.FC<TextboxProps> = ({ className, value, update, placeholder }) => {
	
	const input = (val: string) => String(val).toString()

	return (
		<input 
			type="text" 
			className={getClass("textbox", className)}
			value={value} 
			onChange={(e) => update(input(e.target.value))} 
			placeholder={placeholder}
			readOnly={false}
		/>
  );
}

export default Textbox;