import React from "react";
import getClass from "../../modules/getClass";
import { ButtonProps } from "./Button.types";
import "./Button.scss";

const Button: React.FC<ButtonProps> = ({ className, children, update }) => {
	
	return (
		<input 
			className={getClass("button", className)} 
			onClick={update} 
			value={children} 
			type="button" 
		/>
  );
}

export default Button;