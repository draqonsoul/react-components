import React from "react";
import {Link as ReactLink} from "react-router-dom";
import getClass from "../../modules/getClass";
import Flex from "../Flex/Flex";
import Image from "../Image/Image";
import { NavigationProps } from "./Navigation.types";
import "./Navigation.scss";

const Navigation: React.FC<NavigationProps> = ({ className, children }) => {
	
	return (
		<div 
			className={getClass("navigation", className)}
		>	
			<Flex 
				direction="horizontal"
			>	
				<ReactLink 
					className="link link__medium navigation__link" 
					to="/"
				>
					<Image
						className="navigation__image"
						src="images/home_b.png"
					/>
				</ReactLink>
				{children}
			</Flex>
		</div>
  );
}

export default Navigation;