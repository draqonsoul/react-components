import React from "react";
import {BrowserRouter, Route, Switch, Link as ReactLink} from "react-router-dom";
import Navigation from './Navigation';

export default {
  title: "Container/Navigation"
};

const MyRouter = () => {
  
  return (
    <BrowserRouter className="navigation">
      <Navigation>
        <ReactLink className="link link__medium navigation__link" to="/contacts"> contact </ReactLink>
        <ReactLink className="link link__medium navigation__link" to="/okay"> 404 </ReactLink>
      </Navigation>
      <Switch>
        <Route exact path="/"> Route 1</Route>
        <Route path="/contacts"> Route 2</Route>
        <Route> Route 3</Route>
      </Switch>
    </BrowserRouter>
  )
}

export const Default = () => <MyRouter />