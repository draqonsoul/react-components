import React, {useEffect, useState} from "react";
import Alert from './Alert';

export default {
    title: "Components/Alert"
};

const paragraph = "Text displaying the issue of Example alert. This Alert can be closed by clicking on the 'X' Icon. Lorem Ipsum solom dir Nova prospect in the world of placeholder texts. on this planet its nothing as it seemed so i will just randomly type anything which may or may not befenit society anyways."

const Wrapper: any = ({type, headline, paragraph}: any) => {
    const [alertVisible, setAlertVisible] = useState(true);
    return alertVisible ? (
        <Alert
            type={type}
            headline={headline}
            paragraph={paragraph}
            close={() => setAlertVisible(false)}
        />
    ) : null;
}

export const Info = () => (
    <Wrapper
        type="info"
        headline="Info Message"
        paragraph={paragraph}
    />
)

export const Success = () => (
    <Wrapper
        type="success"
        headline="Success Message"
        paragraph={paragraph}
    />
)

export const Warning = () => (
    <Wrapper
        type="warning"
        headline="Warning Message"
        paragraph={paragraph}
    />
)

export const Error = () => (
    <Wrapper
        type="error"
        headline="Error Message"
        paragraph={paragraph}
    />
)