export interface AlertProps {
  className?: string;
  headline: any;
  paragraph: any;
  close: any;
  type: "error" | "warning" | "success" | "info"
}