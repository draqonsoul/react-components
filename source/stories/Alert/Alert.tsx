import React from "react";
import getClass from "../../modules/getClass";
import Headline from "../Headline/Headline";
import Image from "../Image/Image";
import Paragraph from "../Paragraph/Paragraph";
import { AlertProps } from "./Alert.types";
import "./Alert.scss";

const Alert: React.FC<AlertProps> = ({ className, type, paragraph, headline, close }) => {
	
	return (
		<div 
			className={getClass("alert", className, [[type]])}
		>
			<Headline 
				className="alert__headline" 
				size="small"
			> 
				{headline} 
			</Headline>
			<Paragraph 
				className="alert__paragraph"  
				size="medium"
			> 
				{paragraph}
			</Paragraph>
			<Image
				className="alert__icon"
				src={`svg/close__${type}.svg`}
				onClick={close}
			/>
		</div>
  	);
}

export default Alert;