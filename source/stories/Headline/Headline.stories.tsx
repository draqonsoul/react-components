import React from "react";
import Headline from './Headline';

export default {
  title: "Components/Headline"
};

export const Teaser = () => <Headline size="teaser">Draqons Journey from City 17 to Ravenholm to Nova Prospect</Headline>;
export const XXLarge = () => <Headline size="xxlarge">Draqons Journey from City 17 to Ravenholm to Nova Prospect</Headline>;
export const XLarge = () => <Headline size="xlarge">Draqons Journey from City 17 to Ravenholm to Nova Prospect</Headline>;
export const Large = () => <Headline size="large">Draqons Journey from City 17 to Ravenholm to Nova Prospect</Headline>;
export const Medium = () => <Headline size="medium">Draqons Journey from City 17 to Ravenholm to Nova Prospect</Headline>;
export const Small = () => <Headline size="small">Draqons Journey from City 17 to Ravenholm to Nova Prospect</Headline>;