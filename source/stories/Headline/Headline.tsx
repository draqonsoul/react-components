import React from "react";
import getClass from "../../modules/getClass";
import { HeadlineProps } from "./Headline.types";
import "./Headline.scss";

const Headline: React.FC<HeadlineProps> = ({ className, size, children }) => {
	
	return (
		size === "teaser" ? 
			<h1 
				className={getClass("headline", className, [[size]])} 
			> 
				{children} 
			</h1> 
		:
		size === "xxlarge" ? 
			<h2 
				className={getClass("headline", className, [[size]])} 
			> 
				{children} 
			</h2> 
		:
		size === "xlarge" ? 
			<h3 
				className={getClass("headline", className, [[size]])} 
			> 
				{children} 
			</h3> 
		:
		size === "large" ? 
			<h4 
				className={getClass("headline", className, [[size]])} 
			> 
				{children} 
			</h4> 
		:
		size === "medium" ? 
			<h5 
				className={getClass("headline", className, [[size]])} 
			> 
				{children} 
			</h5> 
		:
		size === "small" ? 
			<h6 
				className={getClass("headline", className, [[size]])} 
			> 
				{children} 
			</h6> 
		: null
  );
}

export default Headline;