export interface HeadlineProps {
  className?: string;
  children: any;
  size: "teaser" | "xxlarge" | "xlarge" | "large" | "medium" | "small";
}