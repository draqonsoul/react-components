import React from "react";
import getClass from "../../modules/getClass";
import { ImageProps } from "./Image.types";
import "./Image.scss";

const Image: React.FC<ImageProps> = ({ className, src, onClick, bordered, scale }) => {
	
	return (
		<img 
			onClick={onClick}
			className={getClass("image", className, [[scale, "scale_down"], [bordered]])}
			src={src}
		/>
  );
}

export default Image;