export interface ImageProps {
  className?: string;
  src: string;
  onClick?: any;
  bordered?: "bordered";
  scale?: "scale_up_down" | "scale_down"
}