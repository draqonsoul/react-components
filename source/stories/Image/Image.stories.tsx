import React from "react";
import Image from './Image';

export default {
  title: "Components/Image"
};

export const ScaleDown = () => <Image scale="scale_down" src="/images/dragon.jpg" />;
export const ScaleFree = () => <Image scale="scale_up_down" src="/images/dragon.jpg" />;
export const Bordered = () => <Image bordered="bordered" src="/images/dragon.jpg" />;