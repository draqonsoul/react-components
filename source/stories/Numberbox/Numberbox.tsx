import React from "react";
import getClass from "../../modules/getClass";
import Image from "../Image/Image";
import Flex from "../Flex/Flex";
import { NumberboxProps } from "./Numberbox.types";
import "./Numberbox.scss";

const Numberbox: React.FC<NumberboxProps> = ({ className, hasButtons, value, min, max, update, placeholder }) => {
	
	const ceil = (val: number, max: number) =>  Number(val) + 1 > max ? max :  Number(val)+1
	const floor = (val: number, min: number) =>  Number(val) - 1 < min ? min :  Number(val)-1
	const input = (val: number, min: number, max: number) => Number(val).toString() === "" ? min : Number(val) > max ? max : Number(val) < min ? min : Number(val)

	return (
		<Flex 
			direction="horizontal"
			className="numberbox__flex_outside"
		>
			<input 
				type="number" 
				className={getClass("numberbox", className)}
				value={value} 
				onChange={(e: any) => update(input(e.target.value, min, max))}
				placeholder={placeholder}
				min={min}
				max={max}
				readOnly={false}
			/>
			{hasButtons ? 
				<Flex 
					direction="vertical"
					className="numberbox__flex"
				>
					<Image 
						className="numberbox__image"
						src={"svg/plus.svg"}
						onClick={() => update(ceil(value, max))}
					/>
					<Image 
						className="numberbox__image"
						src={"svg/minus.svg"}
						onClick={() => update(floor(value, min))}
					/>
				</Flex> 
			: null}
		</Flex>
  );
}

export default Numberbox;