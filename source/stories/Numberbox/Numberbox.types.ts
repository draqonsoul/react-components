export interface NumberboxProps {
  className?: string;
  value: number;
  update?: any;
  placeholder?: any;
  hasButtons?: boolean;
  min: number;
  max: number;
}