import React, { useState } from "react";
import Numberbox from './Numberbox';

export default {
    title: "Inputs/Numberbox"
};

const Wrapper: any = ({hasButtons}: any) => {

    const [value, setValue] = useState(1)

    const min: number = 0
    const max: number = 999

    return (
        <Numberbox
            hasButtons={hasButtons}
            value={value}
            update={(val) => setValue(val)}
            placeholder="Enter a number"
            min={min}
            max={max}
        />
    )
}

export const WithControls = () => <Wrapper hasButtons={true} />;
export const Default = () => <Wrapper hasButtons={false} />;