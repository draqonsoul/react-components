export interface ParagraphProps {
  className?: string;
  children: any;
  size: "teaser" | "medium";
}