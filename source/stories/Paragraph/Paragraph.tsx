import React from "react";
import getClass from "../../modules/getClass";
import { ParagraphProps } from "./Paragraph.types";
import "./Paragraph.scss";

const Paragraph: React.FC<ParagraphProps> = ({ className, size, children }) => {
	
	return (
		<p 
			className={getClass("paragraph", className, [[size]])}
		>
			{children}
		</p>
  );
}

export default Paragraph;