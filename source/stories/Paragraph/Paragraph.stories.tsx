import React from "react";
import Paragraph from './Paragraph';

export default {
  title: "Components/Paragraph"
};

export const Teaser = () => <Paragraph size="teaser"> The Journey was harsh but the Hero eventually found the way out of the dark labyrinth and managed it to escape ... </Paragraph>;
export const Medium = () => <Paragraph size="medium"> The Journey was harsh but the Hero eventually found the way out of the dark labyrinth and managed it to escape ... </Paragraph>;