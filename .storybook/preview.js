import { themes } from '@storybook/theming';

export const parameters = {
  darkMode: {
    // Override the default dark theme
    dark: { ...themes.dark, appBg: 'rgb(42, 42, 42)' },
    // Override the default light theme
    light: { ...themes.normal, appBg: 'rgba(250, 250, 250, 1.0)' }
  }
};