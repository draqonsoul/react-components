const path = require("path");

module.exports = {
  stories: ["../source/**/*.stories.tsx"],
  // Add any Storybook addons you want here: https://storybook.js.org/addons/
  addons: ['storybook-dark-mode'],
  webpackFinal: async (config) => {

    config.module.rules.push({
      test: /\.scss$/,
      use: ["style-loader", "css-loader", {
        loader: "sass-loader",
        options: {
          // Prefer `dart-sass`
          prependData: `@import "public/scss/fontfaces.scss"; @import "public/scss/colors.scss";`,
        },
      }
      ],
      include: path.resolve(__dirname, "../")
    });

    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve("babel-loader"),
      options: {
        presets: [["react-app", { flow: false, typescript: true }]]
      }
    });

    config.module.rules.push({
      test: /\.(png|jp(e*)g|svg|gif)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            outputPath: 'public'
          },
        },
      ],
    });


    config.resolve.extensions.push(".ts", ".tsx");


    return config;
  }
};